package com.pwa.pwa_bindings

import android.Manifest
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

import android.net.Uri
import android.support.v4.content.ContextCompat
import me.aflak.libraries.callback.FingerprintDialogCallback
import me.aflak.libraries.dialog.FingerprintDialog
import android.content.pm.PackageManager
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.telephony.SmsManager
import android.view.View
import android.provider.ContactsContract
import com.google.gson.Gson


/**
 * Created by nasserissifi 15/05/2018
 */

class BridgeAction : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent = intent
        val uri = intent.data
        val ctx = this


        if (uri != null) {
            val path = uri.path
            val pathSegment = uri.pathSegments

            //main url
            val protocol = uri.getQueryParameter("protocol")
            val hostname = uri.getQueryParameter("hostname")
            val port = uri.getQueryParameter("port")
            //parameters
            val datafornative = uri.getQueryParameter("datafornative")//data to native
            val appName = uri.getQueryParameter("appname") // webAPK package name
            val phonenumber = uri.getQueryParameter("phonenumber") // webAPK package name
            val textsms = uri.getQueryParameter("textsms") // webAPK package name
            //hash
            val action = uri.getQueryParameter("action") // what action would you like to perform from native

            Toast.makeText(this, "$protocol//$hostname:$port/?$datafornative&$action/#/nasstore", Toast.LENGTH_SHORT).show()

            when (action) {
                "finger" -> fingerPrintAuthentication(ctx, { resumeWebAppWithData(appName, protocol, hostname, port, "true", action) })
                "contacts" -> {
                    val datatoweb = Gson().toJson(getContactsMap())
                    resumeWebAppWithData(appName, protocol, hostname, port, datatoweb, action)
                } // add getContacts into function
                "sendsms" -> sendSMS(phonenumber, textsms)
            }
        }
    }

    private fun resumeWebAppWithData(appName: String?, protocol: String?, hostname: String?, port: String?, dataforweb: String?, action: String?) {
        val packageName = getPackageNameFromApplicationName(appName)
        if (packageName == "") {
            Toast.makeText(this, "PWA Web apk not found! Make sure to add website to home screen", Toast.LENGTH_SHORT).show()
        } else {
            val launchIntent = packageManager.getLaunchIntentForPackage(packageName).setData(Uri.parse("$protocol//$hostname:$port/?dataforweb=$dataforweb&action=$action/#/nasstore"))
            if (launchIntent != null) startActivity(launchIntent)//null pointer check in case package name was not found
        }
    }

    private fun requestSmsPermission() {
        // Request the permission
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.SEND_SMS), 0)
        // Ask for the read external storage permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.SEND_SMS)) {
                // Provide an additional rationale to the user if the permission was not granted
                // and the user would benefit from additional context for the use of the permission.
                // Display a SnackBar with a button to request the missing permission.
                Snackbar.make(window.decorView, "SMS permission required",
                        Snackbar.LENGTH_INDEFINITE).setAction("OK", View.OnClickListener {
                    // Request the permission
                    ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.SEND_SMS), 0)
                }).show()
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.SEND_SMS), 0)
            }
        }
    }

    private fun requestContactPermission() {
        // Request the permission
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), 0)
        // Ask for the read external storage permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_CONTACTS)) {
                // Provide an additional rationale to the user if the permission was not granted
                // and the user would benefit from additional context for the use of the permission.
                // Display a SnackBar with a button to request the missing permission.
                Snackbar.make(window.decorView, "SMS permission required",
                        Snackbar.LENGTH_INDEFINITE).setAction("OK", {
                    // Request the permission
                    ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), 0)
                }).show()
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), 0)
            }
        }
    }

    private fun fingerPrintAuthentication(ctx: BridgeAction, func: () -> Unit) {
        FingerprintDialog.initialize(this)
                .title("Authentication")
                .message("Authentication message")
                .callback(object : FingerprintDialogCallback {
                    override fun onAuthenticationSucceeded() {
                        Toast.makeText(ctx, "onAuthenticationSucceeded", Toast.LENGTH_SHORT).show()
                        func()
                        finish()
                    }

                    override fun onAuthenticationCancel() {
                        Toast.makeText(ctx, "onAuthenticationCancel", Toast.LENGTH_SHORT).show()
                        finish()
                    }
                }).show()
    }

    //sends an SMS message to another device
    private fun sendSMS(phoneNumber: String, message: String) {
        requestSmsPermission()
        val sms = SmsManager.getDefault()
        sms.sendTextMessage(phoneNumber, null, message, null, null)
    }

    private fun getContactsString(): String {
        requestContactPermission()
        val contacts = getContactMap()
        println(contacts)
        return contacts.toString()
    }

    private fun getContactsMap(): Map<String, String> {
        requestContactPermission()
        return getContactMap()
    }

    //util functions
    private fun getPackageNameFromApplicationName(appName: String?): String? {
        val pm = packageManager
        val packages = pm.getInstalledApplications(PackageManager.GET_META_DATA)
        for (packageInfo in packages) {
            val appNameFound = getApplicationName(packageInfo.packageName)
            if (appName == appNameFound) return packageInfo.packageName
        }
        return ""
    }

    private fun getApplicationName(packageName: String): String {
        val pm = applicationContext.packageManager
        val ai = try {
            pm.getApplicationInfo(packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            null
        }
        return (if (ai != null) pm.getApplicationLabel(ai) else "(unknown)") as String
    }

    private fun getContactMap(): HashMap<String, String> {

        val mapContacts = hashMapOf<String, String>()

        val cr = contentResolver
        val cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)

        if (cur?.count ?: 0 > 0) {
            while (cur != null && cur.moveToNext()) {
                val id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID))
                val name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME))

                if (cur.getInt(cur.getColumnIndex(
                                ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    val pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            arrayOf(id), null)
                    while (pCur!!.moveToNext()) {
                        val phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
//                        if (name.contains("Sydney"))
                            mapContacts[name] = phoneNo
                    }
                    pCur.close()
                }
            }
        }
        cur?.close()
        return mapContacts
    }
}
